if [ -f ~/.bashrc ]; then
   . ~/.bashrc
fi

if [ -f ~/.cargo/env ]; then
   . ~/.cargo/env
fi

if which fastfetch &>/dev/null; then
  fastfetch
elif which neofetch &>/dev/null; then 
  neofetch
fi

