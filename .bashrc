#!/bin/bash

export PATH="$HOME/.local/bin:$HOME/.cargo/bin:$HOME/go/bin:$PATH"
export LD_LIBRARY_PATH="$HOME/.local/lib:$HOME/.local/lib64:$LD_LIBRARY_PATH"
export C_INCLUDE_PATH="$HOME/.local/include:$C_INCLUDE_PATH"
export LANG=en_US.UTF-8

. ~/.bash_prompt
[ -f ~/.git-completion.bash ] && . ~/.git-completion.bash
[ -f "$HOME/.cargo/env" ] && . "$HOME/.cargo/env"
[ -f "$HOME/.bashrc.local" ] && . "$HOME/.bashrc.local"
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
if [ -e /home/linuxbrew/.linuxbrew/bin/brew ]; then
  eval $(/home/linuxbrew/.linuxbrew/bin/brew  shellenv)
fi

# export LS_COLORS='no=00:fi=00:di=01;34:ln=00;36:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=41;33;01:ex=00;32'
# export LS_COLORS='no=00:fi=00:di=01;34:ln=01;36:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=01;05;37;41:mi=01;05;37;41:su=37;41:sg=30;43:tw=30;42:ow=34;42:st=37;44:ex=01;32:'
export EZA_COLORS="da=1;34"
export FZF_COMPLETION_OPTS="--preview 'bat --style=numbers --color=always --line-range :500 {}'"

export PAGER=less
export BAT_STYLE=plain

# Find best editor
for x in nvim vim vi nano ed; do
	if which $x &> /dev/null; then
		export EDITOR=$x
		break
	fi
done

# Find best ls
if eza / &>/dev/null; then
   alias ls='eza'
   alias ll='ls --long --group --header'
   alias lg='ll --git'
else
   alias ls='ls --color=tty --time-style=long-iso'
   alias ll='ls -lh'
fi

if which fzf &> /dev/null; then
  eval "$(fzf --bash)"
  alias ez='$EDITOR $(fzf -m --preview "bat --style=numbers --color=always --line-range :500 {}")'
fi

alias e='$EDITOR'
alias cdg='cd $(findgitroot || echo .)'
alias cdr='cd "$(realpath .)"'
alias mv='mv -i'
alias cp='cp -i'
alias quota='quota -s'
# alias less='less -R'
alias g='rg --color auto --heading --line-number'
function tma() { pkill -u $USER tmux --signal USR1; tmux attach "$@"; }
alias za='zellij attach'
alias disable_mouse_reporting="printf '\e[?1000l'"
alias pbox='podman run --rm -ti --cap-add=SYS_ADMIN --device /dev/fuse registry.gitlab.com/perapp/dotfiles/pbox'
alias perabox='podman run --rm -ti --cap-add=SYS_ADMIN --device /dev/fuse registry.gitlab.com/perapp/dotfiles/perabox'

complete -W "\`grep -oE '^[a-zA-Z0-9_.-]+:([^=]|$)' ?akefile | sed 's/[^a-zA-Z0-9_.-]*$//'\`" make

stty -ixon
[ ! -f "$HOME/.terminfo/w/wezterm" ] && curl -s https://raw.githubusercontent.com/wez/wezterm/master/termwiz/data/wezterm.terminfo | tic -x -o ~/.terminfo -

