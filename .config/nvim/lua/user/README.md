h
j
k
l

f - forward to char
F - left-forward to char
t - till char
T - left-till char
; - repeat last f,F,t,T
, - repeat reversed f,F,t,T

text objects
w - forward to word
W - forward to space-separated word
e - forward to end of word
E - forward to end of space-separated word
b - backwards to word
B - backwards to space-separated word
ge - backwards to end of word
gE - backwards to end of space-separated word
) - forward sentence
( - backward sentence
} - paragraph forward
{ - paragraph backward
]] - forward to section (function, class, ?)
[[ - backward to section
]m - forward to start of method
]M - forward to end of method
[* - forward to comment
[# - forward to #if

gg / gt - goto top
G / gb - goto bottom
gd - goto local declaration
gD - goto global declaration

m{a-zA-Z} - mark position
'{a-zA-Z} - goto mark
'{0-9} - goto previous exit position
'" - goto last edit
'. - goto last change
:marks - marks list

ctrl-o - goto older pos in jump list
ctrl-i - goto newer pos in jumplist
:ju - jump list

zt - scroll so current line at top
zz - scroll so current line in center
zb - scroll so current line in bottom
zh - scroll horizontal to right
zl - scroll horizontal to left

a - append after cursor
A - append after end of line
i - insert at cursor
I - insert at first non blank
o - open line below
O - open line above

x - delete char at cursor
X - delete char before cursor
d{motion} - delete {motion}
dd - delete line(s)
D - delete to end of line(s)
J or _mj_ - join line(s)
gj or _mJ_ - join lines without spaces

yy - yank line(s)
y{motion} - yank {motion}
p - put after cursor
P - put before cursor
"{char} - user register {char} for next delete, yank or put

r - replace char
R - repeat mode
c{motion} - change {motion}
cc - change line(s)
C - change to end of line(s)
s - change character(s)

u - undo
ctrl-r - redo

K or _gm_ - lookup keyword manual (default: "man")

ctrl-l - redraw
ctrl-g - show current file

g~ _m~_ - switch case
gu _mu_ - lowercase
gU _mU_ - uppercase
ctrl-a or _ma_ - add to the number at or after cursor
ctrl-x or _mA_ - subtract to the number at or after cursor
qk{motion} or _mq{motion}_ - format {motion}

v - visual mode
V - linewise visual mode
ctrl-v - blockwise visual mode

## Insert mode
esc/ctrl-[ - exit

bs or ctrl-h or _ctrl-X_ - delete before cursor
del or _ctrl-x_ - delete at cursor
ctrl-u - delete all entered characters

ctrl-o - exec normal mode command
ctrl-w - delete word
ctrl-r - insert register
xtrl-x - complete...

_ctrl-h, ctrl-j, ctrl-k, ctrl-l_ - move char
_ctrl-w, ctrl-b, ctrl-W, ctrl-B, ctrl-e, ctrl-E_ - move word


