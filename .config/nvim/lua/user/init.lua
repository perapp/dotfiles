vim.g.mapleader = " "

require("user.opt")
require("user.keymap")
require("user.lazy")

-- Highlight when yanking (copying) text
--  See `:help vim.highlight.on_yank()`
vim.api.nvim_create_autocmd("TextYankPost", {
	desc = "Highlight when yanking (copying) text",
	group = yank_group,
	callback = function()
		vim.highlight.on_yank({
      timeout = 1000
    })
	end,
})

vim.api.nvim_create_autocmd({'BufRead', 'BufNewFile'}, {
  desc = 'Set filetype for SSH config directory',
  pattern = '*/.ssh/config.d/*',
  command = 'set filetype=sshconfig'
})

vim.api.nvim_create_user_command('Cdf', 'cd %:h', {desc = "cd to dir of current file"})
