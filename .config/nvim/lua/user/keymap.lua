
-- Normal mode
vim.keymap.set("n", "<c-e>", "<End>")
vim.keymap.set("n", "<c-h>", "<Home>")
vim.keymap.set("n", "E", "$", { desc = "End of line" })
vim.keymap.set("n", "B", "^", { desc = "Beginning of text" })
vim.keymap.set("n", "<C-d>", "<C-d>zz", { desc = "Half page down and center" })
vim.keymap.set("n", "<C-u>", "<C-u>zz", { desc = "Half page up and center" })
vim.keymap.set("n", "n", "nzzzv", { desc = "Next match and center" })
vim.keymap.set("n", "N", "Nzzzv", { desc = "Prev match and center" })
vim.keymap.set("n", "<left>", ":lua print('Computer says no...')<cr>")
vim.keymap.set("n", "<up>", ":lua print('Computer says no...')<cr>")
vim.keymap.set("n", "<down>", ":lua print('Computer says no...')<cr>")
vim.keymap.set("n", "<right>", ":lua print('Computer says no...')<cr>")
vim.keymap.set("n", "<Esc>", ":noh<CR>", { desc = "clears search highlights", silent = true })

-- insert mode
function keymap_iset_exec_cmds(cmds)
  for _, cmd in ipairs(cmds) do
    vim.keymap.set("i",
      string.format("<c-%s>", cmd),
      string.format("<c-o>%s", cmd))
  end
end

keymap_iset_exec_cmds({ "h", "j", "k", "l" })
keymap_iset_exec_cmds({ "w", "e", "b" })
keymap_iset_exec_cmds({ "f", "t", ";", "," })
keymap_iset_exec_cmds({ "x" })

-- 'unbind' some g-menu options not used
vim.keymap.set("n", "ga", ":lua print('disabled')<cr>", { desc = "-" }) -- print ascii value of character under the cursor
vim.keymap.set("n", "gh", ":lua print('disabled')<cr>", { desc = "-" }) -- start Select mode
vim.keymap.set("n", "gH", ":lua print('disabled')<cr>", { desc = "-" }) -- start Select line mode
vim.keymap.set("n", "gi", ":lua print('disabled')<cr>", { desc = "-" }) -- like "i", but first move to the |'^| mark
vim.keymap.set("n", "gj", ":lua print('disabled')<cr>", { desc = "-" }) -- like "j", but when 'wrap' on go N screen lines down
vim.keymap.set("n", "gk", ":lua print('disabled')<cr>", { desc = "-" }) -- like "k", but when 'wrap' on go N screen lines up
-- vim.keymap.set("n", "gl", ":lua print('disabled')<cr>", { desc = "-" })
vim.keymap.set("n", "gm", ":lua print('disabled')<cr>", { desc = "-" }) -- go to character at middle of the screenline
vim.keymap.set("n", "gM", ":lua print('disabled')<cr>", { desc = "-" }) -- go to character at middle of the text line
vim.keymap.set("n", "go", ":lua print('disabled')<cr>", { desc = "-" }) -- cursor to byte N in the buffer
vim.keymap.set("n", "gq", ":lua print('disabled')<cr>", { desc = "-" }) -- format Nmove text
vim.keymap.set("n", "gQ", ":lua print('disabled')<cr>", { desc = "-" }) -- switch to "Ex" mode with Vim editing
vim.keymap.set("n", "gr", ":lua print('disabled')<cr>", { desc = "-" }) -- virtual replace N chars with {char}
vim.keymap.set("n", "gR", ":lua print('disabled')<cr>", { desc = "-" }) -- enter Virtual Replace mode
vim.keymap.set("n", "gs", ":lua print('disabled')<cr>", { desc = "-" }) -- go to sleep for N seconds (default 1)
vim.keymap.set("n", "gw", ":lua print('disabled')<cr>", { desc = "-" }) -- format Nmove text and keep cursor
vim.keymap.set("n", "gx", ":lua print('disabled')<cr>", { desc = "-" }) -- execute application for file name under the cursor (only with |netrw| plugin)
vim.keymap.set("n", "gt", ":lua print('disabled')<cr>", { desc = "-" }) -- go to the next tab page
vim.keymap.set("n", "gT", ":lua print('disabled')<cr>", { desc = "-" }) -- go to the previous tab page

-- goto mode
vim.keymap.set("n", "ga", "<c-^>", { desc = "Last accessed buffer" })
vim.keymap.set("n", "gq", ":bdelete<cr>", { desc = "Delete buffer" })
vim.keymap.set("n", "gj", ":bnext<cr>", { desc = "Next buffer" })
vim.keymap.set("n", "gk", ":bprevious<cr>", { desc = "Next buffer" })
vim.keymap.set("n", "gw", ":ls<cr>", { desc = "List buffers" })
vim.keymap.set("n", "gx", ":Explore<cr>", { desc = "Explore files" })
vim.keymap.set("n", "gd", ":lua vim.lsp.buf.definition()<cr>", { desc = "LSP: Goto definition" })
vim.keymap.set("n", "gr", ":lua vim.lsp.buf.references()<cr>", { desc = "LSP: Goto references" })
vim.keymap.set("n", "gw", ":lua require'telescope.builtin'.buffers()<cr>", { desc = "TEL: Buffers" })


vim.keymap.set('n', 'K', '<cmd>lua vim.lsp.buf.hover()<cr>', { desc = 'Hover documentation' })
vim.keymap.set('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<cr>', { desc = 'Go to definition' })
vim.keymap.set('n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<cr>', { desc = 'Go to declaration' })
vim.keymap.set('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<cr>', { desc = 'Go to implementation' })
vim.keymap.set('n', 'go', '<cmd>lua vim.lsp.buf.type_definition()<cr>', { desc = 'Go to type definition' })
vim.keymap.set('n', 'gr', '<cmd>lua vim.lsp.buf.references()<cr>', { desc = 'Go to reference' })
vim.keymap.set('n', 'gs', '<cmd>lua vim.lsp.buf.signature_help()<cr>', { desc = 'Show function signature' })
vim.keymap.set('n', '<F2>', '<cmd>lua vim.lsp.buf.rename()<cr>', { desc = 'Rename symbol' })
vim.keymap.set('n', '<F3>', '<cmd>lua vim.lsp.buf.format({async = true})<cr>', { desc = 'Format file' })
vim.keymap.set('x', '<F3>', '<cmd>lua vim.lsp.buf.format({async = true})<cr>', { desc = 'Format selection' })
vim.keymap.set('n', '<F4>', '<cmd>lua vim.lsp.buf.code_action()<cr>', { desc = 'Execute code action' })


-- mod mode
vim.keymap.set("n", "<leader>ml", "gu", { desc = "Lowercase (gu)" })
vim.keymap.set("n", "<leader>mu", "gU", { desc = "Uppercase (gU)" })

-- files mode
vim.keymap.set("n", "<leader>fx", ":Explore<enter>", { desc = "Explore files" })
vim.keymap.set("n", "<leader>fz", ":edit!<enter>", { desc = "Revert file" })

-- vim config mode
vim.keymap.set("n", "<leader>vc", ":e ~/.config/nvim/lua/user/init.lua<cr>",
  { desc = "Edit config" })

-- LSP actions
vim.keymap.set('n', '<leader>lh', ':lua vim.lsp.buf.hover()<cr>', { desc = "LSP: Show hover information" })
vim.keymap.set('n', '<leader>ld', ':lua vim.lsp.buf.definition()<cr>', { desc = "LSP: Goto definition" })
vim.keymap.set('n', '<leader>lD', ':lua vim.lsp.buf.declaration()<cr>', { desc = "LSP: Declaration" })
vim.keymap.set('n', '<leader>li', ':lua vim.lsp.buf.implementation()<cr>', { desc = "LSP: Implementation" })
vim.keymap.set('n', '<leader>lo', ':lua vim.lsp.buf.type_definition()<cr>', { desc = "LSP: Type definition" })
vim.keymap.set('n', '<leader>lr', ':lua vim.lsp.buf.references()<cr>', { desc = "LSP: Show references" })
vim.keymap.set('n', '<leader>ls', ':lua vim.lsp.buf.signature_help()<cr>', { desc = "LSP: Signature help" })
vim.keymap.set('n', '<leader>lR', ':lua vim.lsp.buf.rename()<cr>', { desc = "LSP: Rename" })
vim.keymap.set({ 'n', 'x' }, '<leader>lf', ':lua vim.lsp.buf.format({async = true})<cr>', { desc = "LSP: Format" })
vim.keymap.set('n', '<leader>la', ':lua vim.lsp.buf.code_action()<cr>', { desc = "LSP: Code action" })
vim.keymap.set('x', '<leader>lA', ':lua vim.lsp.buf.range_code_action()<cr>', { desc = "LSP: Range code action" })

-- Diagnostics
vim.keymap.set('n', '<leader>ll', ':lua vim.diagnostic.open_float()<cr>', { desc = "Diag: Float" })
vim.keymap.set('n', '[d', ':lua vim.diagnostic.goto_prev()<cr>', { desc = "Diag: Goto previous" })
vim.keymap.set('n', ']d', ':lua vim.diagnostic.goto_next()<cr>', { desc = "Diag: Goto next" })

-- tab mode
vim.keymap.set("n", "<leader>to", "<cmd>tabnew<CR>", { desc = "Open new tab" })                     -- open new tab
vim.keymap.set("n", "<leader>tx", "<cmd>tabclose<CR>", { desc = "Close current tab" })              -- close current tab
vim.keymap.set("n", "<leader>tn", "<cmd>tabn<CR>", { desc = "Go to next tab" })                     --  go to next tab
vim.keymap.set("n", "<leader>tp", "<cmd>tabp<CR>", { desc = "Go to previous tab" })                 --  go to previous tab
vim.keymap.set("n", "<leader>tf", "<cmd>tabnew %<CR>", { desc = "Open current buffer in new tab" }) --  move current buffer to new tab
