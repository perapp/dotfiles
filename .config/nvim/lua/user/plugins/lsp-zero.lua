return {
  "VonHeikemen/lsp-zero.nvim",
  branch = "v3.x",
  dependencies = {
    "folke/neodev.nvim",
    "neovim/nvim-lspconfig",
    "williamboman/mason.nvim",
    "williamboman/mason-lspconfig.nvim",

    -- Auto completion
    "hrsh7th/cmp-nvim-lsp",
    "hrsh7th/nvim-cmp",
    "L3MON4D3/LuaSnip",
  },
  config = function()
    local lsp_zero = require('lsp-zero')

    require('mason').setup({})
    require('mason-lspconfig').setup({
      -- list of servers for mason to install
      ensure_installed = {
        "html",
        "cssls",
        "lua_ls",
        "pyright",
        "jdtls",
      },

      handlers = {
        function(server_name)
          require('lspconfig')[server_name].setup({})
        end,
      },
    })

  end,
}
