return {
  "nvim-telescope/telescope.nvim",
  branch = "0.1.x",
  dependencies = {
    "nvim-lua/plenary.nvim",
    { "nvim-telescope/telescope-fzf-native.nvim", build = "make" },
    "nvim-tree/nvim-web-devicons",
    "folke/todo-comments.nvim",
    "nvim-treesitter/nvim-treesitter",
  },
  config = function()
    local telescope = require("telescope")
    local actions = require("telescope.actions")
    local transform_mod = require("telescope.actions.mt").transform_mod

    local trouble = require("trouble")
    local trouble_telescope = require("trouble.sources.telescope")

    -- or create your custom action
    local custom_actions = transform_mod({
      open_trouble_qflist = function(prompt_bufnr)
        trouble.toggle("quickfix")
      end,
    })

    telescope.setup({
      defaults = {
        path_display = { "smart" },
        mappings = {
          i = {
            ["<C-k>"] = actions.move_selection_previous, -- move to prev result
            ["<C-j>"] = actions.move_selection_next, -- move to next result
            ["<C-q>"] = actions.send_selected_to_qflist + custom_actions.open_trouble_qflist,
            ["<C-t>"] = trouble_telescope.open,
          },
        },
      },
    })

    telescope.load_extension("fzf")

    -- set keymaps
    -- vim.keymap.set('n', 'gd', ':Telescope lsp_definitions<cr>', { desc = "TEL: Definition" })
    -- vim.keymap.set('n', 'gi', ':Telescope lsp_implementations<cr>', { desc = "TEL: Implementations" })
    --vim.keymap.set('n', 'gt', require('telescope.builtin').lsp_type_definitions,	{ desc = 'Goto [t]ype Definition' })
    vim.keymap.set('n', 'gd', ':Telescope lsp_definitions<cr>', { desc = "TEL: Goto definition" })
    vim.keymap.set('n', 'gi', ':Telescope lsp_implementations<cr>', { desc = "TEL: Goto implementations" })
    vim.keymap.set("n", "gw", ":lua require('telescope.builtin').buffers({sort_lastused=true})<cr>", { desc = "TEL: find buffers" })
    vim.keymap.set('n', 'gr', "<cmd>Telescope lsp_references<cr>", { desc = 'TEL: References' })
    vim.keymap.set("n", "gw", ":lua require('telescope.builtin').buffers({sort_lastused=true})<cr>", { desc = "TEL: find buffers" })
    vim.keymap.set('n', 'gt', "<cmd>Telescope treesitter<cr>", { desc = 'TEL: Treesitter' })
    vim.keymap.set('n', 'z=', "<cmd>Telescope spell_suggest<cr>", { desc = 'TEL: spelling suggestions' })
    vim.keymap.set("n", "<leader>ff", "<cmd>Telescope find_files<cr>", { desc = "Telescope find files" })
    vim.keymap.set("n", "<leader>fr", "<cmd>Telescope oldfiles<cr>", { desc = "Telescope recent files" })
    vim.keymap.set("n", "<leader>fg", "<cmd>Telescope live_grep<cr>", { desc = "Telescope grep in cwd" })
    vim.keymap.set("n", "<leader>fc", "<cmd>Telescope grep_string<cr>", { desc = "Telescope grep string under cursor in cwd" })
    vim.keymap.set("n", "<leader>vh", "<cmd>Telescope help_tags<cr>", { desc = "Fuzzy find help" })
  end,
}
