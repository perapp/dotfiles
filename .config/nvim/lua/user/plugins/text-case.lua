return {
  "johmsalas/text-case.nvim",
  dependencies = { "nvim-telescope/telescope.nvim" },
  config = function()
    require("textcase").setup({
      prefix = "<leader>m",
    })
    require("telescope").load_extension("textcase")
  end,
  opts = {
    default_keymappings_enabled = false,
  }
}
