return {
  "folke/which-key.nvim",
  init = function()
    vim.o.timeout = true
    vim.o.timeoutlen = 100

    wk = require("which-key")
    wk.add({
      {"g", name = "goto mode" },
      {"<leader>f", name = "+file" },
      {"<leader>m", name = "+modfy" },
      {"<leader>s", name = "+search" },
      {"<leader>v", name = "+vim settings" },
      {"<leader>l", name = "+LSP" },
      {"<leader>w", name = "+swap" },
      {"<leader>t", name = "+tabs" },
    })
  end,
  opts = {
  },
}
