return {
  "AckslD/swenv.nvim",
  dependencies = {
    "nvim-lua/plenary.nvim",
    "ahmedkhalf/project.nvim",
  },
  keys = {
    { "gV", function() require('swenv.api').pick_venv() end, desc = "Pick virtualenv" },
  },
  config = function()
    require("swenv").setup {
      vim.api.nvim_create_autocmd("FileType", {
        pattern = {"python"},
        callback = function()
          require('swenv.api').auto_venv()
        end
      })
    }
  end,
}
