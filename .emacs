; (add-to-list 'load-path "~/.emacs.d/lisp/")

;(require 'color-theme)
;(color-theme-initialize)
;(color-theme-calm-forest)

(setq make-backup-files nil)
(setq vc-make-backup-files nil)
(setq-default indent-tabs-mode nil)
(put 'upcase-region 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(put 'downcase-region 'disabled nil)
(global-set-key (kbd "C-x g") 'goto-line)
(global-set-key (kbd "C-c s") 'isearch-forward-regexp)
; (global-set-key (kbd "C-.") 'dabbrev-completion)
; (global-set-key (kbd "<left>") 'switch-to-buffer)
(put 'set-goal-column 'disabled nil)
(global-set-key "\C-h" 'backward-delete-char)
; (add-hook 'prog-mode-hook 'display-line-numbers-mode)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(inhibit-startup-screen t)
 '(package-selected-packages '("lsp-mode" lsp-mode))
 '(spice-output-local "Gnucap")
 '(spice-simulator "Gnucap")
 '(spice-waveform-viewer "Gwave"))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; Comment/uncomment this line to enable MELPA Stable if desired.  See `package-archive-priorities`
;; and `package-pinned-packages`. Most users will not need or want to do this.
;;(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)
