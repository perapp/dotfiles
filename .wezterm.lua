local wezterm = require 'wezterm'

local config = wezterm.config_builder()

config.color_scheme = 'Catppuccin Mocha'
-- wezterm.plugin.require("https://github.com/nekowinston/wezterm-bar").apply_to_config(config)
config.font_size = 9.0
config.initial_cols = 120
config.initial_rows = 30
config.use_fancy_tab_bar = true
config.enable_tab_bar = false
config.hide_tab_bar_if_only_one_tab = true
config.window_background_opacity = 1.0
-- window_decorations = 'RESIZE'
config.audible_bell = "Disabled"

config.window_close_confirmation = "NeverPrompt"
config.exit_behavior = "CloseOnCleanExit"

config.keys = {
  { key = 's', mods = 'CTRL|ALT', action = wezterm.action.SplitVertical({domain="CurrentPaneDomain"}), },
  { key = 'v', mods = 'CTRL|ALT', action = wezterm.action.SplitHorizontal({domain="CurrentPaneDomain"}), },
  { key = 'x', mods = 'CTRL|ALT', action = wezterm.action.CloseCurrentPane({confirm=false}), },
  -- { key = 'h', mods = 'CTRL|ALT', action = wezterm.action.ActivatePaneDirection('Left'), },
  -- { key = 'j', mods = 'CTRL|ALT', action = wezterm.action.ActivatePaneDirection('Down'), },
  -- { key = 'k', mods = 'CTRL|ALT', action = wezterm.action.ActivatePaneDirection('Up'), },
  -- { key = 'l', mods = 'CTRL|ALT', action = wezterm.action.ActivatePaneDirection('Right'), },
  { key = 'z', mods = 'CTRL|ALT', action = wezterm.action.TogglePaneZoomState, },

  { key = 't', mods = 'CTRL|ALT|SHIFT', action = wezterm.action.ShowLauncherArgs({ flags = 'LAUNCH_MENU_ITEMS|COMMANDS|FUZZY' }), },
  { key = 't', mods = 'CTRL|ALT', action = wezterm.action.ShowLauncher, },
  { key = 'j', mods = 'CTRL|ALT', action = wezterm.action.ActivateTabRelative(1), },
  { key = 'k', mods = 'CTRL|ALT', action = wezterm.action.ActivateTabRelative(-1), },

  { key = 'f', mods = 'CTRL|ALT', action = wezterm.action.ToggleFullScreen, },
  { key = 'Space', mods = 'CTRL|ALT', action = wezterm.action.ActivateCommandPalette, },

  { key = 'c', mods = 'CTRL|ALT', action = wezterm.action.CopyTo('Clipboard'), },
  { key = 'v', mods = 'CTRL|ALT', action = wezterm.action.PasteFrom('Clipboard'), },
}
config.mouse_bindings = {
  -- Ctrl-click will open the link under the mouse cursor
  {
    event = { Up = { streak = 1, button = 'Left' } },
    mods = 'CTRL',
    action = wezterm.action.OpenLinkAtMouseCursor,
  },
}

config.launch_menu = {
  {
    label = 'bash',
    args = { 'bash', '-l' }
  },
  {
    label = 'cmd',
    args = { 'cmd' }
  },
  {
    label = 'PowerShell',
    args = { 'pwsh' }
  },
  {
    label = 'WSL',
    args = { 'wsl' }
  },
  {
    label = 'git bash (win)',
    args = { 'C:\\Program Files\\Git\\bin\\bash.exe', '--cd-to-home' }
  },
  {
    label = 'ssh localhost',
    args = { 'ssh', 'localhost' }
  },
  {
    label = 'ssh dev (debug)',
    args = { 'cmd', '/c', 'ssh', '-v', 'dev', '&', 'pause' }
  },
}
for host, host_config in pairs(wezterm.enumerate_ssh_hosts()) do
  table.insert(config.launch_menu, {
    label = ('ssh ' .. host),
    args = {'ssh', host}
  })
end

-- config.term = "wezterm"
--[[
if package.config:sub(1,1) == '/' then
  -- running linux
  config.default_gui_startup_args = { 'start', 'bash', '-l' }
else
  config.default_gui_startup_args = { 'start', 'cmd' }
end
--]]

return config

