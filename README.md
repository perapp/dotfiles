# dotfiles

These are Pera's dotfiles. To enable them on a new node, run:

    mkdir -p ~/.local/{bin,src}
    git clone https://github.com/TheLocehiliosan/yadm.git ~/.local/src/yadm
    ln -s ~/.local/src/yadm/yadm ~/.local/bin/yadm
    ~/.local/bin/yadm clone https://gitlab.com/perapp/dotfiles.git
    ~/.local/bin/yadm remote set-url origin git@gitlab.com:perapp/dotfiles.git
 
 If on untrusted node, run:

    ~/.local/bin/yadm remote set-url origin https://gitlab.com/perapp/dotfiles.git

## Run as container

pbox

```
podman run --security-opt label=disable --device /dev/fuse --rm -ti --pull=newer registry.gitlab.com/perapp/dotfiles/pbox
```

perabox

```
podman run --security-opt label=disable --device /dev/fuse --rm -ti --pull=newer registry.gitlab.com/perapp/dotfiles/perabox
```

## WSL

There are pre-built WSL images that can be used to get started quickly on Windows. To use them run:

    podman export -o perabox.tar $(podman run --pull always -d --rm registry.gitlab.com/perapp/dotfiles/perabox sleep 5h)
    wsl.exe --unregister perabox
    wsl.exe --import perabox C:\wslDistroStorage\perabox perabox.tar
    wsl.exe -d perabox
    
