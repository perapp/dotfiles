#!/bin/bash

if [ ! -e /dev/mqueue ]; then
    RUN mkdir /dev/mqueue
    RUN mount -t mqueue none /dev/mqueue
fi

