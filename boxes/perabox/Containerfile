FROM quay.io/rockylinux/rockylinux:latest

RUN dnf -y install epel-release
RUN dnf -y install 'dnf-command(config-manager)'
RUN dnf config-manager --set-enabled powertools

RUN dnf -y install langpacks-en glibc-all-langpacks
RUN sed -i -e '/tsflags=nodocs/s/^/# /' /etc/yum.conf /etc/dnf/dnf.conf || true
RUN dnf -y install man-pages man-db sudo dnf-utils fuse fuse-libs

env USER=pera
RUN adduser -G wheel $USER
RUN echo "$USER:$USER" | chpasswd
# RUN echo pera | passwd pera --stdin
RUN echo "$USER ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
RUN /bin/echo -e "[user]\ndefault=$USER" >> /etc/wsl.conf

# Install Podman based on:
# https://github.com/containers/podman/tree/master/contrib/podmanimage/stable
# https://www.redhat.com/sysadmin/podman-inside-container
RUN rpm --setcaps shadow-utils 2>/dev/null
RUN dnf -y install podman crun fuse-overlayfs --exclude container-selinux
ADD --chmod=644 boxes/containers.conf /etc/containers/containers.conf
ADD --chmod=644 boxes/rootless-containers.conf /home/pera/.config/containers/containers.conf
ADD --chmod=644 boxes/storage.conf /etc/containers/storage.conf
RUN mkdir -p /var/lib/shared/{overlay-images,overlay-layers,vfs-images,vfs-layers}
RUN touch /var/lib/shared/{overlay-images/images,overlay-layers/layers,vfs-images/images,vfs-layers/layers}.lock
RUN echo -e "pera:1:999\npera:1001:64535" > /etc/subuid
RUN echo -e "pera:1:999\npera:1001:64535" > /etc/subgid

ADD --chmod=755 boxes/init_wsl.sh /usr/local/bin/

RUN dnf -y install procps sudo passwd curl wget tar bzip2 unzip less
RUN dnf -y install tmux openssl
RUN dnf -y install openssh-clients openssh-server
RUN dnf -y install nano vim
RUN dnf -y install git mercurial make
RUN dnf -y install gcc gcc-c++
RUN dnf -y install python3
RUN python3 -m pip -q install -U pip
RUN dnf -y install java-latest-openjdk
RUN mkdir -p /home/pera/.local/share/containers
RUN chown pera:pera -R /home/pera
VOLUME /var/lib/containers
VOLUME /home/pera/.local/share/containers

RUN chown -R $USER:$USER /home/$USER
USER $USER
WORKDIR /home/$USER

# Install yadm
RUN mkdir -p ~/.local/bin ~/.local/src
RUN git clone https://github.com/TheLocehiliosan/yadm.git ~/.local/src/yadm
RUN ln -nfs ~/.local/src/yadm/yadm ~/.local/bin/yadm
ENV PATH=$PATH:~/.local/bin

# Install Homebrew
RUN /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
ENV PATH=$PATH:~/../linuxbrew/.linuxbrew/bin

# Add and run home_init
ADD --chown=$USER .local/bin/home_init /tmp/home_init
RUN /tmp/home_init

# Run Yadm clone
# ADD --chown=$USER .local/share/yadm/repo.git /tmp/dotfiles/.git
ADD --chown=$USER . /tmp/dotfiles
RUN .local/bin/yadm clone --no-bootstrap /tmp/dotfiles
RUN .local/bin/yadm checkout .
ADD --chown=$USER boxes/yadm.patch /tmp/
RUN .local/bin/yadm remote set-url origin https://gitlab.com/perapp/dotfiles.git
RUN if [ -s /tmp/yadm.patch ]; then .local/bin/yadm apply /tmp/yadm.patch; fi

RUN .local/bin/home_init
CMD bash --login

